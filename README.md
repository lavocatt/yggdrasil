Original code : https://gitlab.inria.fr/tlavocat/stage_M2

# Installation

### installing the lib in dev mode :

In project root directory
```
pip3 install -e .
```

# Requirements

### Needed softwares/libraries :

* taktuk `sudo aptitude install taktuk`
* python3 `sudo aptitude install python3`
* pyzmq `sudo aptitude install python3-zmq`
* pexpect `sudo aptitude install python3-pexpect`

# Local configuration for dev purposes

## SSH config file containing :

```
Host A
    StrictHostKeyChecking no                                                    
    HashKnownHosts no 
    HostName        A
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host B
    StrictHostKeyChecking no                                                    
    HashKnownHosts no 
    HostName        B
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host C
    StrictHostKeyChecking no                                                    
    HashKnownHosts no 
    HostName        C
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host D
    StrictHostKeyChecking no                                                    
    HashKnownHosts no 
    HostName        D
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host E
    StrictHostKeyChecking no                                                    
    HashKnownHosts no 
    HostName        E
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
Host F
    StrictHostKeyChecking no                                                    
    HashKnownHosts no 
    HostName        F
    IdentityFile    /home/$USERNAME$/.ssh/passord_less_key
```

Replacing $USERNAME$ by your user name.

## /etc/hosts containing :

```
127.0.1.1	A
127.0.1.1	B
127.0.1.1	C
127.0.1.1	D
127.0.1.1	E
127.0.1.1	F
127.0.1.1	G
127.0.1.1	H
127.0.1.1	I
127.0.1.1	J
127.0.1.1	K
127.0.1.1	L
127.0.1.1	M
```

## SSH Config


### /etc/ssh/sshd_config: (on the remote end)

Ensure no max session is set.

# Unit tests

```
python3 setup.py test
```

### Using VENV :

In project root directory :
```
virtualenv venv
source venv/bin/activate
```

