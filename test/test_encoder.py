import pytest
from yggdrasil.erebor.encoder import MPIDecoder

def test_mpi_decoder():
    d = MPIDecoder()
    l = ["a", "b", "cd", "efghij", "a"]
    e = d.pack(*l)
    assert(d.unpack(e) == l)
