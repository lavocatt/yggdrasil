import base64
import sys
import json
from ..  import consts
from .executor  import Executor

class SerialBroadcaster(Executor):

    def __init__(self, name, commands, group, end=False, timeout=-1,
            fail_threshold=0, encoding=consts.encoding) :
        Executor.__init__(self, group, end, name, timeout, encoding)
        self.commands = commands
        self.commandsw= len(commands)
        self.fail_threshold = fail_threshold
        self.nbfail = 0

    def runing_state(self) :
        self.exec_one()

    def exec_one(self) :
        try :
            command = self.commands.pop()
            self.framework.broadcast_exec_on("0",
                                   command,
                                   consts.TRUE,
                                   "0",
                                   self.group.ID,
                                   self.group.root_r,
                                   self.group.root_n,
                                   self.command_done)
        except :
            pass

    # to override
    def command_done(self, data) :
        decoded_data = json.loads(data)
        stdouts = decoded_data[consts.STDOUT]
        stderrs = decoded_data[consts.STDERR]
        statuss = decoded_data[consts.STATUS]
        for key, value in statuss.items() :
            status = base64.b64decode(statuss[key][0]).decode(self.encoding)
            stdout = []
            stderr = []
            if key in stdouts :
                stdout = stdouts[key]
            if key in stderrs :
                stderr = stderrs[key]
            self.printt_decoded_data(stderr, stdout, statuss[key][0], key)
            exstat = status.split("Exited with status")
            stat   = int(exstat[len(exstat)-1])
            if stat != 0 :
                self.nbfail += 1
        self.commandsw -=1
        if self.commandsw == 0 :
            if self.nbfail <= self.fail_threshold :
                self.request_transition(consts.RUNNING_DONE)
            else :
                self.request_transition(consts.RUNNING_ERROR)
        else :
            self.exec_one()
