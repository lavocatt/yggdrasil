import re
import json
from .. import consts
from .task     import Task
from .mpi_jail import MPIJail

class MessageReceiver(Task):
    """
    MessageReceiver task aims to connect to a local group, let's say root. And
    listen for generic messages, if those messages respect a certain pattern
    they will trigger a specified callback function.
    The callback function needs to return True or False.
    """

    def __init__(self, name, pattern, callback, local_ID, timeout=-1) :
        """
        Create a new MessageReceiver

        :param name:     (string) Task name, must be unique
        :param pattern:  (string) regex pattern to trigger the callback function
        :param callback: (func)   callback function returning True if the task
                                  must end
        :param local_ID: (string) the group on which listen to generic messages
        :param timeout:  (int)    set to a time in seconds if the task need to
                                  end by itself after a given time period.
        """
        Task.__init__(self, name, timeout)
        self.callback      = callback
        self.local_ID      = local_ID
        self.message_regex = re.compile(pattern)

    def runing_state(self) :
        self.network = self.framework.get_network(self.local_ID)
        self.network.register_on_bridge_generic_messages(self.receive_gmsg)

    def receive_gmsg(self, data) :
        if self.state == consts.RUNNING :
            decoded_message = json.loads(data)
            decoded_data    = json.loads(decoded_message[consts.DATA])
            if self.message_regex.match(decoded_data[consts.DATA]) != None :
                if self.callback(decoded_data[consts.DATA]) :
                    self.request_transition(consts.RUNNING_DONE)
