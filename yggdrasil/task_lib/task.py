from threading import Timer
from .. import consts
from .. import Event

class Dependency :
    AND = 1
    OR  = 0
    def __init__(self, logic_operator, listDependencies) :
        self.logic_operator = logic_operator
        self.listDependencies = listDependencies

    def update(self, task, transition) :
        for sub_dependency in self.listDependencies :
            sub_dependency.update(task, transition)

    def can_run(self) :
        if self.logic_operator == Dependency.AND :
            b = 1
            for sub_dependency in self.listDependencies :
                b = b * sub_dependency.can_run()
        else :
            b = 0
            for sub_dependency in self.listDependencies :
                b = b + sub_dependency.can_run()
            if b > 1 :
                b = 1
        return b

    def return_all_tasks(self) :
        tasks = []
        for sub_dependency in self.listDependencies :
            for task in sub_dependency.return_all_tasks() :
                tasks.append(task)
        return tasks


class DependencyEvent :

    def __init__(self, task, transition) :
        self.task = task
        self.transition = transition
        self.state = 0

    def update(self, task, transition) :
        if self.task == task and self.transition == transition :
            self.state = 1

    def can_run(self) :
        return self.state

    def return_all_tasks(self) :
        return [self.task]

class Task:

    def __init__(self, name, timeout=-1, root_r="0", root_n="root"):
        self.name          = name            # Way to identify the node
        self.root_r        = root_r          # Node where root erebor is running
        self.root_n        = root_n          # Name of the root erebor instance
        self.state         = consts.IDLE     # init state
        self.on_state      = []
        self.on_transition = []
        # Keep track of the list of tasks from who we listen on notifications
        self.listen_on      = []
        self.listen_on_state= []
        # for transitions
        self.dependant_transitions = dict()
        self.dependant_state       = dict()
        self.waiting_callback_on_possible_transition = dict()
        self.waiting_transitions  = []
        self.waiting_state  = []
        self.trigger_transitions  = dict()
        self.trigger_states  = dict()
        self.timer         = None
        if timeout > -1 :
            #print("timer launcher")
            self.timer = Timer(timeout,self.timeout_task)
        self.run_dependency    = list()
        self.cancel_dependency = list()

    def set_framework(self, framework):
        self.framework = framework

    # callback registration ---------------------------------------------------
    def register_on_state(self, callback) :
        self.on_state.append(callback);
        return self

    def register_on_transition(self, callback) :
        self.on_transition.append(callback);
        return self

    # Transitions -------------------------------------------------------------

    def set_dependency_for_transition(self, transition, dependency) :
        self.dependant_transitions[transition] = dependency
        for task in dependency.return_all_tasks() :
            task.register_on_transition(self.upon_foreign_transition);

    def get_dependency_for_transition(self, transition) :
        if transition in self.dependant_transitions :
            return self.dependant_transitions[transition]
        else :
            return None

    def set_trigger_for_transition(self, transition, dependency) :
        self.trigger_transitions[transition] = dependency
        for task in dependency.return_all_tasks() :
            task.register_on_transition(self.upon_foreign_transition);

    def get_trigger_for_transition(self, transition) :
        if transition in self.trigger_transitions :
            return self.trigger_transitions[transition]
        else :
            return None

    def upon_foreign_transition(self, task, transition) :
        toRemove = []
        for my_transition, dependency in self.dependant_transitions.items() :
            dependency.update(task, transition)
            if dependency.can_run()  == 1:
                toRemove.append(my_transition)
        for my_transition in toRemove :
            del self.dependant_transitions[my_transition]
            if my_transition in self.waiting_transitions :
                self.waiting_transitions.remove(my_transition)
                self.request_transition(my_transition)
            if my_transition in self.waiting_callback_on_possible_transition :
                callback = self.waiting_callback_on_possible_transition[transition]
                del self.waiting_callback_on_possible_transition[transition]
                callback()
        toRemove = []
        for my_transition, dependency in self.trigger_transitions.items() :
            dependency.update(task, transition)
            if dependency.can_run() == 1 :
                toRemove.append(my_transition)
        for my_transition in toRemove :
            del self.trigger_transitions[my_transition]
            arrival_state = my_transition - self.state
            if (arrival_state    == consts.INIT
                or arrival_state == consts.RUNNING
                or arrival_state == consts.DONE
                or arrival_state == consts.ERROR
                or arrival_state == consts.TIMEOUT
                or arrival_state == consts.CANCELED ):
                self.apply_transition(my_transition)

    def execute_when_transition_transition_possible(self, transition, callback) :
        """
        Will execute the given callback when the transition is possible.

        Used when a task needs to do special work before entering a transition
        to a special state.

        Used especially for groups : When a group needs to terminate, some
        (maybe time consuming and asynchronous) code
        needs to be executed before arriving to the DONE state.
        """
        if (    transition is consts.IDLE_INIT or
                transition is consts.IDDLE_CANCEL or
                transition is consts.INIT_RUNNING or
                transition is consts.INIT_ERROR or
                transition is consts.INIT_CANCELED or
                transition is consts.RUNNING_DONE or
                transition is consts.RUNNING_ERROR or
                transition is consts.RUNNING_TIMEOUT or
                transition is consts.RUNNING_CANCELED) :
            if self.check_dependencies_for_transition(transition) :
                callback()
            else :
                self.waiting_callback_on_possible_transition[transition]=callback
        else :
            print("{} !!!!!!!!!!! invalid transition asked {}".format(self.name, transition))

    def request_transition(self, transition) :
        """
        will try to apply the asked transition to the current task, if a
        dependency is blocking the transition, it will wait until all needed
        transitions on the requested tasks are done to be triggered
        """
        if (    transition is consts.IDLE_INIT or
                transition is consts.IDDLE_CANCEL or
                transition is consts.INIT_RUNNING or
                transition is consts.INIT_ERROR or
                transition is consts.INIT_CANCELED or
                transition is consts.RUNNING_DONE or
                transition is consts.RUNNING_ERROR or
                transition is consts.RUNNING_TIMEOUT or
                transition is consts.RUNNING_CANCELED) :
            if self.check_dependencies_for_transition(transition) :
                self.apply_transition(transition)
            else :
                self.waiting_transitions.append(transition)
        else :
            print("{} !!!!!!!!!!! invalid transition asked {}".format(self.name, transition))

    def check_dependencies_for_transition(self, transition) :
        arrival_state = transition - self.state
        # execute state code
        #print("{} {}".format(self.name, self.dependant_transitions))
        if transition not in self.dependant_transitions :
            if arrival_state == consts.INIT :
                return True
            elif arrival_state == consts.RUNNING :
                return True
            elif arrival_state == consts.DONE :
                return True
            elif arrival_state == consts.ERROR :
                return True
            elif arrival_state == consts.TIMEOUT :
                return True
            elif arrival_state == consts.CANCELED :
                return True
        return False

    def apply_transition(self, transition) :
        """
        apply the given transition
        * trigger the transition notification before the new state is applied
        * trigger the state notification once the new state is applied
        * after the new state setup, call the state function for sub classes to
           execute real work
        """
        arrival_state = transition - self.state
        # notify changes
        on_transition = self.state + arrival_state
        self.norify_transition(on_transition)
        self.state = arrival_state
        self.norify_state(self.state)
        # execute state code
        if arrival_state == consts.INIT :
            self.init_state();
        elif arrival_state == consts.RUNNING :
            # Start the timer upon Running state
            self.launch_timer();
            self.runing_state();
        elif arrival_state == consts.DONE :
            if self.timer != None :
                self.timer.cancel()
            self.done_state();
        elif arrival_state == consts.ERROR :
            if self.timer != None :
                self.timer.cancel()
            self.error_state();
        elif arrival_state == consts.TIMEOUT :
            if self.timer != None :
                self.timer.cancel()
            self.timeout_state();
        elif arrival_state == consts.CANCELED :
            if self.timer != None :
                self.timer.cancel()
            self.canceled_state();

    # Methods below are to be overridden by sub classes. ----------------------
    def init_state(self) :
        self.request_transition(consts.INIT_RUNNING)
        pass

    def runing_state(self) :
        self.request_transition(consts.RUNNING_DONE)
        pass

    def done_state(self) :
        pass

    def error_state(self) :
        pass

    def timeout_state(self) :
        pass

    def canceled_state(self) :
        pass


    # Timer handling ----------------------------------------------------------
    def launch_timer(self, name=""):
        if self.timer != None :
            self.timer.start()

    def timeout_task(self) :
        self.framework.erebor.add_to_queue(Event(self._timeout, self.name))

    def _timeout(self, name="") :
        # Only trigger a timeout for non done tasks.
        if self.state is consts.RUNNING :
            self.request_transition(consts.RUNNING_TIMEOUT)

    # event propagation -------------------------------------------------------
    def norify_state(self, state) :
        for callback in self.on_state :
            callback(self, state)

    def norify_transition(self, transition) :
        for callback in self.on_transition :
            callback(self, transition)
