import json
from .. import consts
from .executor import Executor
from .mpi_jail import MPIJail

class MPIExecutor(Executor):

    def __init__(self, name, command, jail, end=False, timeout=-1,
                                                    encoding=consts.encoding,
                                                    long_opt=False,
                                                    exec_rank="0") :
        Executor.__init__(self, jail, end, name, timeout, encoding)
        self.exec_rank = exec_rank;
        if long_opt :
            self.command = "{} --NETWORK={} --PORT={}".format(command,
                                                    self.group.ID,
                                                    self.group.server_number)
        else :
            self.command = "{} -N {} -P {}".format(command, self.group.ID,
                                                   self.group.server_number)

    def runing_state(self) :
        self.framework.exec_on(self.exec_rank,
                self.command,
                consts.TRUE,
                "0",
                self.group.ID,
                self.group.root_r,
                self.group.root_n,
                self.command_done)

    # to override
    def command_done(self, data) :
        self.print_data(data, self.group.ID)
        #self.framework.end_mpi_session(self.group.ID,
        #    self.group.root_r,
        #    self.group.root_n)
        self.request_transition(consts.RUNNING_DONE)
