import json
from .. import consts
from .numbered_group import NumberedGroup

class MPIJail(NumberedGroup):

    server_number = 6000

    def __init__(self, name, ID, node_list, base_rank, base_network, tcp=False, timeout=-1):
        NumberedGroup.__init__(self, name, ID, node_list, base_rank,
                               base_network, timeout)
        MPIJail.server_number+=1
        self.server_number = MPIJail.server_number
        self.server_socket = "{}".format(self.server_number)
        self.is_ipc = consts.TRUE
        if tcp :
            self.is_ipc = consts.FALSE

    def start_numbered_group(self) :
        def mpi_session_started(data) :
            self.start_mpi_jail_group()
        self.framework.start_mpi_session(self.ID,
                self.root_r,
                self.root_n,
                self.server_socket,
                mpi_session_started,
                self.is_ipc)

    # to override
    def start_mpi_jail_group(self) :
        self.request_transition(consts.INIT_RUNNING)

    def terminate_task(self) :
        self.execute_when_transition_transition_possible(self.state+consts.DONE,
                self.kill_mpi)

    def kill_mpi(self):
        self.framework.end_mpi_session(self.ID, self.root_r,self.root_n)
        self.kill_group()
