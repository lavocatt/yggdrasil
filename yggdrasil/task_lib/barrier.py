import re
import json
from .. import consts
from .message_receiver   import MessageReceiver

class Barrier(MessageReceiver):
    """
    The Barrier task will end a group if a certain amount of messages validating
    the regex are received.
    """

    def __init__(self, name, pattern, group_to_end, local_ID, how, timeout=-1) :
        """
        Create a new Barrier

        :param name:         (string) Task name, must be unique
        :param pattern:      (string) regex pattern to trigger the callback
                                      function
        :param group_to_end: (Group)  group to end
        :param local_ID:     (string) the group on which listen to generic
                                      messages
        :param timeout:       (int)   set to a time in seconds if the task need
                                      to end by itself after a given time
                                      period.
        """
        MessageReceiver.__init__(self, name, pattern, self.trigger,
                                 local_ID)
        self.group_to_end  = group_to_end
        self.how           = how

    def canceled_state(self):
        if self.end :
            self.group.terminate_task()
    def error_state(self):
        if self.end :
            self.group.terminate_task()
    def timeout_state(self) :
        if self.end :
            self.group.terminate_task()
    def done_state(self):
        if self.end :
            self.group.terminate_task()

    def trigger(self, data) :
        print(data)
        self.how = self.how -1
        return self.how == 0
