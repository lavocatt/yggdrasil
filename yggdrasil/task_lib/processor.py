#!/usr/bin/env python3
import getopt
import time
import json
import sys
import signal
import threading
from yggdrasil.erebor   import FrameworkControler
from yggdrasil.erebor   import Erebor
from .. import consts

class TaskProcessor(FrameworkControler):
    def __init__(self, erebor, ID, node_list, tfile=None):
        FrameworkControler.__init__(self, erebor, ID, tfile)
        self.job    = {}
        self.tasks  = []
        self.register_tasks()
        for task in self.tasks :
            task.set_framework(self)
            self.job[task] = 1
            task.register_on_state(self.task_state)
            #task.register_on_transition(self.task_transition)

    # to override
    def register_tasks(self) :
        pass

    def end(self, task) :
        self.job[task] = 0
        to_wait = sum(self.job.values())
        if to_wait == 0 :
            self.erebor.terminate()

    def task_transition(self, task, transition) :
        # Print a colored log
        if transition == consts.IDLE_INIT :
            print(consts.blue("{} transit {}".format(task.name, "idle -> init")))
        elif transition == consts.IDDLE_CANCEL :
            print(consts.blue("{} transit {}".format(task.name, "idle -> cancel")))
        elif transition == consts.INIT_RUNNING :
            print(consts.blue("{} transit {}".format(task.name, "init -> running")))
        elif transition == consts.INIT_ERROR :
            print(consts.blue("{} transit {}".format(task.name, "init -> error")))
        elif transition == consts.INIT_CANCELED :
            print(consts.blue("{} transit {}".format(task.name, "init -> cancel")))
        elif transition == consts.RUNNING_DONE:
            print(consts.blue("{} transit {}".format(task.name, "running -> done")))
        elif transition == consts.RUNNING_TIMEOUT :
            print(consts.orange("{} transit {}".format(task.name, "running -> timeout")))
        elif transition == consts.RUNNING_CANCELED :
            print(consts.orange("{} transit {}".format(task.name, "running -> canceled")))
        elif transition == consts.RUNNING_ERROR :
            print(consts.error("{} transit {}".format(task.name, "running -> error")))
        else:
            print(consts.error("{} transit {}".format(task.name, "{} Unknown and its a major issue".format(transition))))

    def task_state(self, task, state) :
        # Print a colored log
        if state == consts.IDLE :
            print(consts.blue("{} is {}".format(task.name, "idle")))
        elif state == consts.INIT :
            print(consts.blue("{} is {}".format(task.name, "init")))
        elif state == consts.RUNNING :
            print(consts.blue("{} is {}".format(task.name, "running")))
        elif state == consts.DONE :
            print(consts.green("{} is {}".format(task.name, "done")))
        elif state == consts.TIMEOUT :
            print(consts.orange("{} is {}".format(task.name, "timeout")))
        elif state == consts.CANCELED :
            print(consts.orange("{} is {}".format(task.name, "canceled")))
        elif state == consts.ERROR :
            print(consts.error("{} is {}".format(task.name, "error")))
        else:
            print(consts.error("{} is {}".format(task.name, "Unknown and its a major issue")))
        # If the task is in a terminal state, decrement its wait value and
        # terminate if necessary
        if state >= consts.DONE :
            self.end(task)

    # Run some tests
    def start(self, networkId):
        self.networkId = networkId
        print("processor start")
        for task in self.tasks :
            task.request_transition(consts.IDLE_INIT)

    # Add a task on the fly
    def add_task(self, task) :
        task.set_framework(self)
        self.job[task] = 1
        task.register_on_state(self.task_state)
        #task.register_on_transition(self.task_transition)
        task.request_transition(consts.IDLE_INIT)

    def add_start_all(self, tasks) :
        for task in tasks :
            task.set_framework(self)
            self.job[task] = 1
            task.register_on_state(self.task_state)
        for task in tasks :
            task.request_transition(consts.IDLE_INIT)
