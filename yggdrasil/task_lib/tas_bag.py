import base64
import json
from ..          import consts
from .task       import Task
from .task       import Dependency
from .task       import DependencyEvent

class TaskBag(Task) :

    def __init__(self, name, task_list=None, timeout=-1, encoding=consts.encoding) :
        Task.__init__(self, name, timeout)
        self.final_state = consts.DONE
        self.wait_done = 0
        if task_list is not None :
            self.set_task_list(task_list)
        else :
            self.task_list = []

    def add_task(self, task) :
        self.task_list.append(task)
        self.wait_done += 1
        task.register_on_state(self.sub_task_state_change)
        dep      = DependencyEvent(task, consts.INIT_RUNNING)
        prev_dep = self.get_dependency_for_transition(consts.INIT_RUNNING)
        if prev_dep is None :
            self.set_dependency_for_transition(consts.INIT_RUNNING, Dependency(Dependency.AND, [dep]))
        else :
            self.set_dependency_for_transition(consts.INIT_RUNNING, Dependency(Dependency.AND, [dep, prev_dep]))

    def runing_state(self) :
        pass

    def sub_task_state_change(self, task, state) :
        if state is consts.DONE :
            self.wait_done -=1
        if state is consts.CANCELED or state is consts.ERROR :
            self.wait_done -=1
            if self.final_state is consts.DONE :
                self.final_state = state
                for task in self.task_list :
                    if (task.state is not consts.CANCELED and task.state is not
                            consts.ERROR and task.state is not consts.DONE):
                        task.request_transition(task.state + consts.CANCELED)
        if self.wait_done is 0 :
            self.request_transition(self.state + self.final_state)

    def set_all_dependency_for_transition(self, transition, dependency) :
        self.set_dependency_for_transition(transition, dependency)
        for t in self.task_list :
            t.set_dependency_for_transition(transition, task_t, task)

    def set_all_trigger_for_transition(self,transition, dependency) :
        self.set_trigger_for_transition(transition, dependency)
        for t in self.task_list :
            t.set_trigger_for_transition(transition, dependency)
