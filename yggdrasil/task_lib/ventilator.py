import base64
import json
from ..              import consts
from .executor       import Executor
from .numbered_group import NumberedGroup

class Ventilator(Executor):

    def __init__(self, name, commands, group, end=False, timeout=-1,
            fail_threshold=0, encoding=consts.encoding, exec_rank="") :
        Executor.__init__(self, group, end, name, timeout, encoding)
        self.commands = commands
        self.exec_rank= exec_rank
        self.to_exec  = len(commands)
        self.busy_map = dict()
        self.nbfail   = 0
        self.fail_threshold = fail_threshold

    def runing_state(self) :
        def got_spawn_list(data) :
            decoded_data = json.loads(data)
            spawn_list   = decoded_data[consts.DATA]
            if self.exec_rank == "" :
                for spawn in spawn_list :
                    self.busy_map[spawn] = []
            else :
                self.busy_map[self.exec_rank] = []
            while self.assign_a_task() :
                pass
        self.framework.acquire_spawn_list(self.group.ID,self.group.root_r,
                                          self.group.root_n,got_spawn_list)

    def assign_a_task(self) :
        try :
            # find the next free computer
            free_rank ="" 
            for rank, task_list in self.busy_map.items() :
                if len(task_list) == 0 :
                    free_rank = rank
                    break;
            if free_rank == "" :
                return False
            command = self.commands.pop()
            self.busy_map[free_rank] = command
            self.framework.exec_on(free_rank,
                                   command,
                                   consts.TRUE,
                                   "0",
                                   self.group.ID,
                                   self.group.root_r,
                                   self.group.root_n,
                                   self.command_done)
            return True
        except :
            return False

    # to override
    def command_done(self, data) :
        decoded = json.loads(data)
        status = base64.b64decode(decoded[consts.STATUS]).decode(self.encoding)
        exstat = status.split("Exited with status")
        stat   = int(exstat[len(exstat)-1])
        if stat != 0 :
            self.nbfail += 1
        #E-1: uptime (27515): status > Exited with status 0"
        identity= status.split(": ")[0]
        # the last dash separate the taktuk id
        sp = identity.split("-")
        rank    = sp[len(sp) - 1]
        self.print_data(data, self.name+"  "+identity)
        self.busy_map[rank] = []
        self.to_exec -= 1
        self.assign_a_task()
        if self.to_exec == 0 :
            if self.nbfail <= self.fail_threshold :
                self.request_transition(consts.RUNNING_DONE)
            else :
                self.request_transition(consts.RUNNING_ERROR)
