import json
import base64
from .task     import Task
from .group    import Group
from .. import consts

class Executor(Task) :
    """
    Abstract class
    """

    def __init__(self, on, end, name, timeout=-1, encoding=consts.encoding) :
        Task.__init__(self, name, timeout)
        self.group    = on
        self.end      = end
        self.encoding = encoding

    def canceled_state(self):
        if self.end :
            self.group.terminate_task()
    def error_state(self):
        if self.end :
            self.group.terminate_task()
    def timeout_state(self) :
        if self.end :
            self.group.terminate_task()
    def done_state(self):
        if self.end :
            self.group.terminate_task()

    def print_data(self, data, prefix) :
        decoded = json.loads(data)
        self.printt_decoded_data(decoded["stderr"], decoded["stdout"],
                                 decoded["status"], prefix)

    def printt_decoded_data(self, stderr, stdout, status, prefix) :
        print("---------- {} ----------".format(prefix))
        print("-> Task : {} ".format(self.name))
        value = stderr
        for line in value :
            line = base64.b64decode(line).decode(self.encoding)
            line = line.split("error >")[1]
            print ("{} {} : {}".format(prefix, "stderr", line))
        value = stdout
        for line in value :
            line = base64.b64decode(line).decode(self.encoding)
            line = line.split("output >")[1]
            print ("{} {} : {}".format(prefix, "stdout", line))
        print ("{} {} : {}".format(prefix, "status",
            base64.b64decode(status).decode(self.encoding)))
