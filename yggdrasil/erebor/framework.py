import time
import json
import sys
import os.path
from .. import consts

class FrameworkControler:

    def __init__(self, erebor, ID, filename):
        self.erebor = erebor
        self.ID     = ID
        self.tfile  = None
        self.instant= time.time()

        print("path "+str(filename))
        if filename != None :
            if os.path.isfile(filename) :
                self.tfile = open(filename, 'a')
            else :
                self.tfile = open(filename, 'w')
                self.tfile.write("time;type\n")

    def close(self) :
        if self.tfile != None :
            self.tfile.close()

    def log_time(self, t):
        if self.tfile != None :
            new = time.time()
            self.tfile.write(str(new-self.instant)+";"+t+"\n")

    # spawn a network
    def bootstrap(self):
        self.erebor.bootstrap(self.ID, "", self.log_time)

    # To override
    def start(self, networkId, node_list):
        pass

    def get_network(self, networkId) :
        return self.erebor.networks.get(networkId)

    # Spawn nodes with final_dest on dnetid network as root
    #
    # params
    #   final_dest : node number on dnetid network
    #   nodes      : comma separated node list
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def spawn_on(self, final_dest, nodes, synchro, dnode, dnetid, snode,snetid,
                                                                  callback=None):
        id_callback = -1
        if callback != None :
            id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps({
            consts.ACTION:consts.SPAWN,
            consts.DEST:final_dest,
            consts.DATA:nodes,
            consts.CALLBACK:str(id_callback),
            consts.SYNCHRO:synchro})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Start a new group on
    #
    # params
    #   ID         : name of the new group
    #   options    : options
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def new_group_on(self, ID, options, dnode, dnetid, snode,snetid):
        message = json.dumps({
            consts.ACTION:consts.TAKTUK,
            consts.ID:ID,
            consts.OPTIONS:options})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Execute command on final_dest on dnetid network. dnode ask for execution
    #
    # params
    #   final_dest : node number on dnetid network
    #   nodes      : comma separated node list
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def exec_on(self, final_dest, command, synchro, dnode, dnetid, snode,
                                                        snetid, callback=None):
        id_callback = -1
        if callback != None :
            id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps({
            consts.ACTION:consts.EXECUTE,
            consts.DEST:final_dest,
            consts.DATA:command,
            consts.CALLBACK:str(id_callback),
            consts.SYNCHRO:synchro})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Execute command on final_dest on dnetid network. dnode ask for execution
    #
    # params
    #   final_dest : node number on dnetid network
    #   nodes      : comma separated node list
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def broadcast_exec_on(self, final_dest, command, synchro, dnode, dnetid,
                                                snode, snetid, callback=None):
        id_callback = -1
        if callback != None :
            id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps({
            consts.ACTION:consts.BR_EXEC,
            consts.DEST:final_dest,
            consts.DATA:command,
            consts.CALLBACK:str(id_callback),
            consts.SYNCHRO:synchro})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Wait reduce on dnode@dnetid
    #
    # params
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def renumber_on(self, synchro, dnode, dnetid, snode, snetid, callback=None):
        id_callback = -1
        if callback != None :
            id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps(
                {consts.ACTION:consts.NNUMBER,consts.SYNCHRO:synchro,
                    consts.CALLBACK:str(id_callback)})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Wait reduce network dnetid
    #
    # params
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def wait_reduce_on(self, target, synchro, dnode, dnetid, snode, snetid):
        message = json.dumps({
            consts.ACTION:consts.WAITR,
            consts.TARGET:target,consts.SYNCHRO:consts.TRUE})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Network update on dnode@dnetid
    #
    # params
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def network_update_on(self, synchro, dnode, dnetid, snode, snetid,
                                                                 callback=None):
        id_callback = -1
        if callback != None :
            id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps({
            consts.ACTION:consts.NUPDATE,consts.SYNCHRO:synchro,
            consts.CALLBACK:str(id_callback)})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Make dnode on dnetId send a trans_message to fdnode on fdnetid
    #
    # params
    #   message    : package to send for final_destination
    #   fdnode     : final destination node
    #   fdnetid    : final destination network
    #   fsnode     : final source node
    #   fsnetid    : final source network
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def tnet_msg_on(self, message, fdnode, fdnetid, fsnode, fsnetid,
                                                  dnode, dnetid, snode, snetid):
        final_message = json.dumps({
                        consts.SNODE:fsnode,
                        consts.SNETID:fsnetid,
                        consts.DNODE:fdnode,
                        consts.DNETID:fdnetid,
                        consts.ACTION:consts.TNETWRK,
                        consts.DATA:message})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  final_message)

    # Make dnode on dnetId send a mpi_trans_message to fdnode_rank on fdnetid
    #
    # params
    #   message    : package to send for final_destination
    #   fdnode     : final destination mpi rank
    #   fdnetid    : final destination network
    #   fsnode     : final source node
    #   fsnetid    : final source network
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def mpi_tnet_msg_on(self, message, fdnode, fdnetid, fsnode, fsnetid,
                                                  dnode, dnetid, snode, snetid):
        final_message = json.dumps({
                        consts.SNODE:fsnode,
                        consts.SNETID:fsnetid,
                        consts.DNODE:fdnode,
                        consts.DNETID:fdnetid,
                        consts.ACTION:consts.MTNETWRK,
                        consts.DATA:message})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  final_message)

    # Make dnode replicate Erebor on final_dest on network dnetid. The
    # replication trigger an immediate network creation on final_dest
    #
    # params
    #   final_dest : the node who will start Erebor
    #   ID         : the name of the new network started by final_dest
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def replic_on(self, final_dest, ID, options, dnode, dnetid, snode, snetid):
        message = json.dumps({
            consts.ACTION:consts.REPLIC,
            consts.ID:ID,
            consts.OPTIONS:options,
            consts.DEST:final_dest})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Make dnode terminate Erebor on final_dest on network dnetid. The
    # terminaison is recursive
    #
    # params
    #   final_dest : the node who will terminate Erebor
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def terminate(self, final_dest, dnode, dnetid, snode, snetid):
        message = json.dumps({
            consts.ACTION:consts.TERMHIM,
            consts.DEST:final_dest})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Make dnode on dnetid kill the network ID
    #
    #
    # params
    #   ID         : the network that will be terminated
    #   synchro    : the tree need to be synchronized or not for this action
    #   dnode      : the node who will aply the order
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def delete_network(self, ID, dnode, dnetid, snode, snetid):
        message = json.dumps({
            consts.ACTION:consts.TERMNET,
            consts.ID:ID})
        self.erebor.send_trans_network_message_to(dnode,
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Open a mpi com container on every node of the group pointed. Any of the
    # nodes deployed on this group should be already deployed because this
    # implies a replication on each of them.
    #
    # params
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    #   port       : the port fort zmq on the hook
    #   callback   : the function who will be executed at the end of te
    #                operation
    def start_mpi_session(self, dnetid, snode, snetid, port, callback, is_ipc=consts.TRUE):
        id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps({
            consts.ACTION:consts.MPIDEPL,
            consts.PORT:port,
            consts.CALLBACK:str(id_callback),
            consts.NETWORK:dnetid,
            consts.ISIPC:is_ipc})
        self.erebor.send_trans_network_message_to("0",
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    # Close the mpi com container on every node of the group pointed.
    #
    # params
    #   dnetid     : the network of the node who will apply the order
    #   snode      : the node who emit the order
    #   snetid     : the network of the node who emit the order
    def end_mpi_session(self, dnetid, snode, snetid):
        message = json.dumps({
            consts.ACTION:consts.MPICLN,
            consts.NETWORK:dnetid})
        self.erebor.send_trans_network_message_to("0",
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)

    def acquire_spawn_list(self, dnetid, snode, snetid, callback) :
        id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps({
            consts.ACTION:consts.GETSPWN,
            consts.NETWORK:dnetid,
            consts.CALLBACK:str(id_callback),
            })
        self.erebor.send_trans_network_message_to("0",
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)


    def acquire_spawn_errors(self, dnetid, snode, snetid, callback) :
        id_callback = self.erebor.register_execute_callback(callback)
        message = json.dumps({
            consts.ACTION:consts.GETSPWE,
            consts.NETWORK:dnetid,
            consts.CALLBACK:str(id_callback),
            })
        self.erebor.send_trans_network_message_to("0",
                                                  dnetid,
                                                  snode,
                                                  snetid,
                                                  message)
