import logging
import sys
import json
from .. import consts
from ..isengard.isengardc import Isengard
from ..consts import bcolors

logger = logging.getLogger('yggdrasil')


class Network():

    def __init__(self, add_to_queue, root, ID, debug_list, loglv, logf,
                 taktuk_path, options="", log_time=None):
        self.debug_list      = debug_list
        self.debug           = "network" in self.debug_list
        self.loglv           = loglv
        self.logf            = logf
        self.ID              = ID
        self.child_callbacks = []
        self.order_callbacks = []
        self.ntwrk_callbacks = []
        self.mtrsn_callbacks = []
        self.trsnt_callbacks = []
        self.gmsg_callbacks  = []
        self.state           = consts.INIT
        self.connector = Isengard(add_to_queue, root, debug_list, loglv, logf,
                                  taktuk_path,
                                  self.bridge_internal_callback,
                                  self.bridge_isengard_callback,
                                  self.bridge_generic_message_callback,
                                  self.fire_network_ready_callback,
                                  self.kill_needed,
                                  options, log_time)

    # Registration ############################################################

    def register_on_bridge_generic_messages(self, values):
        self.gmsg_callbacks.append(values)

    def register_mpi_trans_network_callback(self, register_callback):
        self.mtrsn_callbacks.append(register_callback)

    def register_trans_network_callback(self, register_callback):
        self.trsnt_callbacks.append(register_callback)

    # network bootstrap
    def register_network_bootstrap_callback(self, register_callback):
        self.ntwrk_callbacks.append(register_callback)

    def register_order_callback(self, register_callback):
        self.order_callbacks.append(register_callback)

    def register_children_callback(self, register_callback):
        self.child_callbacks.append(register_callback)

    # Events ##################################################################

    # When an order pop
    def new_mpi_trans_network_has_come(self, order, rank):
        self.call_mpi_transnet_callbacks(order, rank)

    # When an order pop
    def new_trans_network_has_come(self, order, rank):
        self.call_transnet_callbacks(order, rank)

    # When an order pop
    def new_order_has_come(self, order, rank):
        self.call_order_callbacks(order, rank, self.ID)

    # When a child pop
    def new_child_has_come(self, rank, ID):
        self.call_connection_callbacks(rank, ID)

    def bridge_generic_message_callback(self, txt):
        logger.info("receive generic bridge message {}".format(txt))
        self.call_gmsg_callbacks(txt)

    # is called when something arrive from the bridge
    def bridge_internal_callback(self, obj):
        if obj[consts.TYPE] == consts.INFOS:
            # Look for rank information
            self.rank = obj[consts.RANK]
            self.connector.rank = obj[consts.RANK]
            logger.debug("my rank is "+self.rank)
            # If not network_master, forward the information
            self.notify_bootstrap(self.rank != "0")
        elif obj[consts.TYPE] == consts.QUIT:
            self.new_order_has_come(json.loads(consts.QUIT_ORDER), "0")
        # Display some logs
        else:
            logger.error(bcolors.FAIL + "-> Bridge log: "+obj[
                         consts.DATA]+bcolors.ENDC)

    def bridge_isengard_callback(self, sender, decoded_data):
        if decoded_data[consts.TYPE] == consts.INFOS:
            # Get children's info
            # It means a new one as shown
            # transfert request
            # TODO get target info
            self.new_child_has_come(
                    decoded_data[consts.RANK],
                    decoded_data[consts.ID])
        # If it is an order
        elif decoded_data[consts.TYPE] == consts.ORDER:
            self.new_order_has_come(decoded_data, sender)
        # If it is a trans-network communication
        elif decoded_data[consts.TYPE] == consts.TNETWRK:
            self.new_trans_network_has_come(decoded_data, sender)
        elif decoded_data[consts.TYPE] == consts.MTNETWRK:
            self.new_mpi_trans_network_has_come(decoded_data, sender)

    def fire_network_ready_callback(self, error_nodes):
        pass

    def kill_needed(self, n):
        self.new_order_has_come(json.loads(consts.QUIT_ORDER), "0")
        sys.exit(-1)

    # Propagation ##############################################################

    # Notify my bootstrap:
    # -> to the registered callbacks
    # -> to my network master (if it exsit: propagate = true)
    def notify_bootstrap(self, propagate):
        self.state = consts.RUNNING
        data  = json.dumps({
            consts.FROM: self.rank,
            consts.TYPE: consts.INFOS,
            consts.RANK: self.rank,
            consts.ID: self.ID
            })
        if propagate:
            self.connector.send_message_to("0", "all", data, False)
        self.call_ntwrk_callbacks(data)

    # network bootstrap
    def call_ntwrk_callbacks(self, order):
        for callback in self.ntwrk_callbacks:
            callback(order)

    def call_order_callbacks(self, order, rank, ID):
        for callback in self.order_callbacks:
            callback(order, rank, ID)

    def call_mpi_transnet_callbacks(self, order, rank):
        for callback in self.mtrsn_callbacks:
            callback(order, rank)

    def call_transnet_callbacks(self, order, rank):
        for callback in self.trsnt_callbacks:
            callback(order, rank)

    def call_connection_callbacks(self, rank, ID):
        for callback in self.child_callbacks:
            callback(rank, ID)

    def call_gmsg_callbacks(self, txt):
        for c in self.gmsg_callbacks:
            c(txt)
