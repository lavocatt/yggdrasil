import logging
import json
import zmq
from .. import consts
from .encoder import MPIDecoder
from threading import Thread
from .. import Event

logger = logging.getLogger('yggdrasil')

class MPIHook(Thread) :

    def __init__(self, add_to_queue, trans_hook, mpi_trans_hook, port, network,
                 debug_list, loglv, logf, is_ipc) :
        Thread.__init__(self)
        self.debug_list   = debug_list
        self.decoder      = MPIDecoder()
        self.debug_list      = debug_list
        self.loglv           = loglv
        self.logf            = logf
        self.debug           = "mpi" in self.debug_list
        self.context      = zmq.Context()
        self.poll         = True
        self.subscriber   = dict()
        self.network      = network
        self.port         = port
        self.add_to_queue = add_to_queue
        self.daemon       = True
        self.send_trans_network_message_to     = trans_hook
        self.send_mpi_trans_network_message_to = mpi_trans_hook
        self.is_ipc = is_ipc == consts.TRUE

    def run(self) :
        # A context per thread
        context    = zmq.Context()
        rep_socket = context.socket(zmq.PULL)
        logger.debug(consts.bold("mpi -> Deploying, ussing port {} is IPC ? {}".format(self.port, self.is_ipc)))
        if self.is_ipc :
            rep_socket.bind("ipc:///tmp/{}".format(self.port))
        else :
            rep_socket.bind("tcp://*:{}".format(self.port))
        logger.debug(consts.bold("mpi -> reply socket ready to receive on {}").format(self.port))
        poller = zmq.Poller()
        poller.register(rep_socket)
        while self.poll: # TODO do actual polling pls
            try :
                logger.debug(consts.bold("wait to receive"))
                events = dict(poller.poll(10000))
                if events[rep_socket] == zmq.POLLIN :
                    rcv_message = rep_socket.recv()
                    logger.debug(consts.bold("received"))
                    logger.debug(consts.bold("received {}".format(rcv_message)))
                    decoded_message = self.unpack(rcv_message)
                    logger.debug(consts.bold("unpacked {}".format(decoded_message)))
                    # a new process is registering to me
                    if decoded_message[consts.TYPE] == consts.REG :
                        logger.debug(consts.bold("mpi -> new registration to do later"))
                        self.add_to_queue(Event(self.new_registration, decoded_message))
                    # a process wants to send a message to another one
                    if decoded_message[consts.TYPE] == consts.MPISEND :
                        logger.debug(consts.bold("mpi -> send a message later"))
                        self.add_to_queue(Event(self.send_message_to_distant_mpi_rank,
                                                decoded_message))
                    if decoded_message[consts.TYPE] == consts.MESSAGE :
                        logger.debug(consts.bold("mpi -> send a ctrl message later"))
                        self.add_to_queue(Event(self.send_message_to_distant_erebor,
                                                decoded_message))
                    # unlock the state of request socket
                    logger.debug(consts.bold("mpi -> done loop"))
                else :
                    logger.debug(consts.bold("no messages"))
            except Exception as e:
                logger.warning(consts.bold("{}".format(e)))

    def unpack(self, to_unpack) :
        fields = self.decoder.unpack(to_unpack)
        logger.debug(consts.bold("unpack {}".format(fields)))
        if fields[0] == consts.REG :
            return {
                    consts.TYPE:consts.REG,
                    consts.RANK:fields[1],
                    consts.PORT:fields[2]
                }
        else :
            logger.debug(consts.bold(fields[0]))
            return {
                consts.TYPE:fields[0],
                consts.DNODE:fields[1],
                consts.DNETID:fields[2],
                consts.SNODE:fields[3],
                consts.SNETID:fields[4],
                consts.DATA:fields[5]
                };

    def is_destination_for(self, rank) :
        return rank in self.subscriber

    def new_registration(self, decoded_message) :
        # register the rank
        rank   = decoded_message[consts.RANK]
        logger.info(consts.bold("-> new registration {}".format(rank)))
        socket = self.context.socket(zmq.PUSH)
        if self.is_ipc :
            socket.connect("ipc:///tmp/{}".format(decoded_message[consts.PORT]))
        else :
            socket.connect("tcp://127.0.0.1:{}".format(decoded_message[consts.PORT]))
        self.subscriber[rank] = socket
        # advertise the root node
        message = json.dumps({consts.ACTION:consts.MPIREG,consts.RANK:rank})
        self.send_trans_network_message_to("0",
                                           self.network.ID,
                                           self.network.rank,
                                           self.network.ID,
                                           message)

    def send_message_to_local_mpi_rank(self, mpi_rank, dnode, dnetid, message) :
        logger.info(consts.bold("mpi -> found rank to send data"))
        socket = self.subscriber[mpi_rank]
        socket.send(self.decoder.pack(dnode, dnetid, message))
        logger.debug(consts.bold("mpi -> data sent"))

    def send_message_to_distant_mpi_rank(self, decoded_message) :
        dnode = decoded_message[consts.DNODE]
        dnet  = decoded_message[consts.DNETID]
        snode = decoded_message[consts.SNODE]
        snet  = decoded_message[consts.SNETID]
        data  = decoded_message[consts.DATA]
        logger.info(consts.bold("mpi -> request sending mpi trans network message"))
        self.send_mpi_trans_network_message_to(dnode,dnet,snode,snet,data)

    def send_message_to_distant_erebor(self, decoded_message) :
        dnode = decoded_message[consts.DNODE]
        dnet  = decoded_message[consts.DNETID]
        snode = decoded_message[consts.SNODE]
        snet  = decoded_message[consts.SNETID]
        data  = decoded_message[consts.DATA]
        message = json.dumps({
            consts.ACTION:consts.MESSAGE,
            consts.DEST:"0",
            consts.TARGET:"all",
            consts.DATA:data,
            consts.SYNCHRO:consts.FALSE
            })
        logger.info(consts.bold("mpi -> request sending a trans network message"))
        self.send_trans_network_message_to(dnode,dnet,snode,snet,message)

    def kill(self) :
        self.poll = False
