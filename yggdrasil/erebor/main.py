#!/usr/bin/env python3
import argparse
import logging
import sys
import signal
import yggdrasil
from yggdrasil.erebor import Erebor
from ..log import configure_logger
import sys, traceback, threading, time

logger = logging.getLogger('yggdrasil')

def argless_runner(Controler, logv="CRITICAL"):

    log = ["erebor", "network", "isengard", "wrapper",
           "unix_socket", "bridge", "mpi"]
    configure_logger(logger, log, logv, None)
    erebor = Erebor(True, log, logv, "", "", "")
    def signal_handler(a, b):
        erebor.terminate()
    signal.signal(signal.SIGINT, signal_handler)
    controler = Controler(erebor, "root", "", "")
    # Start the test sample after the root network bootstrap
    erebor.on_network_init("root", controler.start)
    controler.bootstrap()
    erebor.process_messages()
    erebor.terminate()

def runner(argv, Controler=None):
    print("start")
    parser = argparse.ArgumentParser(description='Main Erebor launcher')
    parser.add_argument(
            "-n",
            "--non-root",
            action="store_false",
            default=True,
            help='''Precise if this instance is user controled or not
            Non supposed to be manually set''')
    parser.add_argument(
            "-i",
            "--id",
            default="root",
            help='''ID of the instance
            Non supposed to be manually set'''
            )
    parser.add_argument(
            "-d",
            "--debug",
            default="erebor,network,isengard,wrapper,unix_socket,bridge,mpi",
            help='''Set which parts needs to output debug infos (comma
            separated
            Goes along with debug-level and debug-file'''
            )
    parser.add_argument(
            "-l",
            "--debug-level",
            default="CRITICAL",
            help='''\
            Debug precision level.
            To be choosed in the following list :
            -------------------------------------
            NOTSET DEBUG INFO WARNING ERROR CRITICAL
            '''
            )
    parser.add_argument(
            "-F",
            "--debug-file",
            default="",
            help='''Path where write the logs.
            Will be the same on every nodes'''
            )
    parser.add_argument(
            "-t",
            "--time-file",
            help='''
            Give a file name where to checkpoint some execution points.
            Can be used in the user code using self.time_log(...)'''
            )
    parser.add_argument(
            "-H",
            "--host-list",
            default="",
            help='''Specifies a comma separated node list to log on to.
            Can also be specified on a file -> file-host'''
            )
    parser.add_argument(
            "-f",
            "--file-list",
            default="",
            help='''Specifies a file containing nodes to log on.
            A node per line'''
            )
    parser.add_argument(
            "-D",
            "--taktuk-deploy",
            default=False,
            action="store_true",
            help="Specifies if taktuk need to deploy itself on the ndoes",
            )
    parser.add_argument(
            "-E",
            "--erebor-deploy",
            action="store_true",
            default=False,
            help='''Specifies if erebor need to deploy itself on the ndoes.
            Non yet implemented'''
            )
    parser.add_argument(
            "-P",
            "--taktuk-path",
            default="",
            help='''Path where to find TakTuk executable.
            If set, this path needs to be the same on each connected
            computer'''
            )
    parser.add_argument(
            "-p",
            "--erebor-path",
            default="",
            help='''Path where to find Erebor executable.
            Not implemented'''
            )

    try:
        args = parser.parse_args()
        #print(args)
        root = args.non_root
        ID   = args.id
        hlist= args.host_list
        tfile= args.time_file
        epath= args.erebor_path
        tpath= args.taktuk_path
        deple= args.erebor_deploy
        deplt= args.taktuk_deploy
        log  = args.debug
        loglv= args.debug_level
        logf = args.debug_file

        # Custom operations for parameters
        # Transform * to a list of loggers
        if log == "*" :
            log = ["erebor", "network", "isengard", "wrapper",
            "unix_socket", "bridge", "mpi"]
        else :
            log = log.split(",")

        # Parse the host file
        hosts = args.file_list
        if hosts != "" :
            f = open(hosts, 'r')
            hlist = ",".join(f.readlines())
            hlist = hlist.replace("\n", "")
            hlist = hlist.replace("\r", "")
            f.close()
        # Remove unecessary /
        if epath != "" and not epath.endswith("/") :
            epath = epath+"/"
        if tpath != "" and not tpath.endswith("/") :
            tpath = tpath+"/"

        # Create logger
        configure_logger(logger, log, loglv, logf)
        logger.info("Option parsing done")
        erebor = Erebor(root, log, loglv, logf, epath, tpath)

        def signal_handler(a, b):
            erebor.terminate()

        signal.signal(signal.SIGINT, signal_handler)

        if root:
            controler = Controler(erebor, ID, hlist, tfile)
            # Start the test sample after the root network bootstrap
            erebor.on_network_init(ID, controler.start)
            controler.bootstrap()
        else:
            erebor.bootstrap(ID)
        erebor.process_messages()
        erebor.terminate()
    except:
        logger.fatal("Unexpected error: {}".format())
        erebor.terminate()
        sys.exit(-1)


def main() :
    logger.setLevel(logging.INFO)
    runner(sys.argv)
