import json

#constants
ISIPC   = "ISIPC"
GETSPWE = "get spawn errors"
GETSPWN = "get spawn"
STOPMPI = "stop mpi bridge"
STRTMPI = "start mpi bridge"
PORT    = "port"
MPISEND = "mpi send"
MPICLN  = "mpi clean"
MPIDEPL = "mpi deploy communicator"
MPIREG  = "mpi register"
REG     = "register"
RANKS   = "ranks"
BULKR   = "bulk replic"
DEAD    = "dead node"
MYRANK  = "my rank"
BRIDGED = "bridge dead"
HEARTBTR= "heart beat reply"
HEARTBT = "heart beat"
SEQ     = "seq"
OPTIONS = "options"
STATUS  = "status"
STDERR  = "stderr"
STDOUT  = "stdout"
CALLBACK= "callback"
TERMNET = "terminate network"
TERMHIM = "terminate children"
DNETID  = "destination network"
DNODE   = "destination node"
SNETID  = "source network"
SNODE   = "source node"
RSHTABLE= "request table"
SHTABLE = "share table"
VISIBLTY= "visibility"
CREAT   = "creation"
SPANTR  = "spanning tree"
ACTION  = "action"
REPLIC  = "replicate"
INVJSON = "Invalid JSON"
MTNETWRK= "mpi trans network"
TNETWRK = "trans network"
TAKTUK  = "taktuk"
ACKQUIT = "ack-quit"
QUIT    = "quit"
ORDER   = "order"
WAITR   = "wait reduce"
NNUMBER = "network renumber"
NUPDATE = "network update"
SYNCHRO = "synchronize"
NETWORK = "network"
SPAWN   = "spawn"
INTERNAL= "internal"
RANK    = "rank"
INFOS   = "infos"
BR_EXEC = "broadcast exec"
EXECUTE = "execute"
MESSAGE = "message"
DEST    = "dest"
TARGET  = "target"
COMMAND = "command"
DATA    = "data"
FROM    = "from"
LENGTH  = "length"
CONTROL = "control"
ACK     = "ack"
TYPE    = "type"
LOG     = "log"
VALUE   = "value"
ID      = "id"

UINT32  = 4
TRUE    = "true"
FALSE   = "false"

# routing rules

PROTECTED = "protected"
PRIVATE   = "private"
PUBLIC    = "public"

# static JSON

DEMAND_INFOS   = json.dumps({TYPE:CONTROL,VALUE:INFOS});
QUIT_ORDER     = json.dumps({TYPE:ORDER,ORDER:QUIT});
ACK_QUIT_ORDER = json.dumps({TYPE:ORDER,ORDER:ACKQUIT});
HB             = json.dumps({TYPE:HEARTBT});
HBR            = json.dumps({TYPE:HEARTBTR});
REQUEST_TABLES = json.dumps({ACTION:RSHTABLE})

#parameters
encoding = "utf-8"
timeout  = 30
heartBeat= 30
MAXQ     = 500

#states
IDLE     = 1
INIT     = 2
RUNNING  = 4
DONE     = 40
ERROR    = 50
TIMEOUT  = 60
CANCELED = 70


#Authorized transitions
IDLE_INIT           = IDLE+INIT
IDDLE_CANCEL        = IDLE+CANCELED
INIT_RUNNING        = INIT+RUNNING
INIT_ERROR          = INIT+ERROR
INIT_CANCELED       = INIT+CANCELED
RUNNING_DONE        = RUNNING+DONE
RUNNING_ERROR       = RUNNING+ERROR
RUNNING_TIMEOUT     = RUNNING+TIMEOUT
RUNNING_CANCELED    = RUNNING+CANCELED
TERMINAISON_PENDING = "terminaison_pending"

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    WHITE = '\x1B[37m'

def bold(str) :
    return bcolors.BOLD+str+bcolors.ENDC

def blue(str) :
    return bold(bcolors.OKBLUE+str+bcolors.ENDC)

def green(str) :
    return bold(bcolors.OKGREEN+str+bcolors.ENDC)

def orange(str) :
    return bold(bcolors.WARNING+str+bcolors.ENDC)


def error(str) :
    return bold(bcolors.FAIL+str+bcolors.ENDC)
