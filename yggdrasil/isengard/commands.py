import re
import logging
import json
from .. import consts

logger = logging.getLogger('yggdrasil')


class RunningCommand:
    def __init__(self, name, dest, command, debug, loglv, logf,
                 callback_object=None):
        self.name    = name
        self.dest    = dest
        self.command = command
        self.stdout  = []
        self.stderr  = []
        self.finall  = ""
        self.wait    = 1
        self.debug   = debug
        self.loglv   = loglv
        self.logf    = logf
        self.stdout_regex = self.get_stdout_regex()
        self.stderr_regex = self.get_stderr_regex()
        self.final_regex  = self.get_final_regex()
        self.callback_object = callback_object

    def test_and_dispatch(self, txt_utf8, txt_base_64):
        logger.info(txt_utf8);
        if self.stdout_regex.match(txt_utf8) is not None:
            self.append_stdout(txt_utf8, txt_base_64)
            return 0
        elif self.stderr_regex.match(txt_utf8) is not None:
            self.append_stderr(txt_utf8, txt_base_64)
            return 1
        elif self.final_regex.match(txt_utf8) is not None:
            self.final(txt_utf8, txt_base_64)
            if self.wait == 0:
                return 2
            else:
                return 0
        else:
            return -1

    def get_stdout_regex(self):
        regex = "^.*{}-{}: {} \(.*\): output >.*".format(self.name,
                self.dest, re.escape(self.command))
        logger.debug("regex -> ^.*{}-{}: {} \(.*\):" " output >.*".format(self.name,
                self.dest, re.escape(self.command)))
        return re.compile(regex)

    def get_stderr_regex(self):
        regex = "^.*{}-{}: {} \(.*\): error >.*".format(self.name,
                self.dest, re.escape(self.command))
        logger.debug("regex -> ^.*{}-{}: {} \(.*\):"
                     " error >.*".format(self.name, self.dest, self.command))
        return re.compile(regex)

    def get_final_regex(self):
        regex = "^.*{}-{}: {} \(.*\): status >.*".format(self.name,
                self.dest, re.escape(self.command))
        logger.debug("regex -> ^.*{}-{}: {} \(.*\):"
                     " status >.*".format(self.name, self.dest, self.command))
        return re.compile(regex)

    def append_stdout(self, line, line_64):
        self.stdout.append(line_64)

    def append_stderr(self, line, line_64):
        self.stderr.append(line_64)

    # handle here ?
    def node_deads(self, error_nodes, errors_rank):
        logger.warning("error nodes {}".format(errors_rank))
        for i in range(0, len(error_nodes)):
            rank = "X"
            if i < len(errors_rank):
                rank = errors_rank[i]
            logger.warning("comp '{}' '{}'".format(rank, self.dest))
            if self.dest == "[0-9]*" or rank in self.dest:
                logger.error("dead")
                self.final("dead")
                if self.wait == 0:
                    return 2
                else:
                    return 1
        return 0

    def final(self, line, line64):
        self.wait -= 1
        self.finall = line64
        if self.callback_object is not None:
            logger.debug(" TO WARN {} ".format(self.callback_object))
            data = json.dumps({
                consts.STDOUT: self.stdout,
                consts.STDERR: self.stderr,
                consts.STATUS: self.finall
                })
            self.callback_object.fire(data)
        else:
            self.print_self(self.stdout, self.stderr, self.finall)

    def print_self(self, stdout, stderr, status):
        for s in stdout:
            logger.info(bcolors.OKGREEN+"COMMAND output > "+s+bcolors.ENDC)
        for s in stderr:
            logger.info(bcolors.OKGREEN+"COMMAND error > "+s+bcolors.ENDC)
        logger.info(bcolors.OKGREEN+"COMMAND status > "+status+bcolors.ENDC)


class BrodcastRunningCommand(RunningCommand):
    def __init__(self, dest_list, command, debug, logv, logf, callback_object):
        RunningCommand.__init__(self, "", "[0-9]*", command, debug, logv, logf,
                                callback_object.copy_with_another_callback(
                                    self.done))
        self.wait = len(dest_list)-1
        self.end_out = []
        self.bstatus = dict()
        self.bstdout = dict()
        self.bstderr = dict()
        self.my_callbac_object = callback_object
        logger.debug("broadcast wait {}".format(self.wait))
        logger.info("dest to wait broadcast command {}".format(self.wait))

    def append_stdout(self, line, line_64):
        identity = line.split(":")[0]
        if identity not in self.bstdout:
            self.bstdout[identity] = []
        self.bstdout[identity].append(line_64)

    def append_stderr(self, line, line_64):
        identity = line.split(":")[0]
        if identity not in self.bstderr:
            self.bstderr[identity] = []
        self.bstderr[identity].append(line_64)

    def append_status(self, line, line_64):
        identity = line.split(":")[0]
        if identity not in self.bstatus:
            self.bstatus[identity] = []
        self.bstatus[identity].append(line_64)

    def final(self, line, line_64):
        self.wait -= 1
        self.append_status(line, line_64)
        self.callback_object.fire("")

    def done(self, data, snode, snetid, dnode, dnetid, value):
        if self.wait == 0:
            final = json.dumps({
                consts.STDOUT: self.bstdout,
                consts.STDERR: self.bstderr,
                consts.STATUS: self.bstatus
                })
            self.my_callbac_object.fire(final)
