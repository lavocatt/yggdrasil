import base64
import logging
import json
import time
import socket
import re
from threading            import Lock
from threading            import Timer
from ..                   import consts
from .unix_socket_wrapper import Unix_socket
from .socket_bridge       import Bridge
from .taktuk_wrapper      import Wrapper
from ..consts             import bcolors
from ..                   import Helper
from ..                   import Event
from .commands            import BrodcastRunningCommand
from .commands            import RunningCommand

logger = logging.getLogger('yggdrasil')

HOST     = 0
PEER     = 1
POSITION = 1
RANK     = 2
LINE     = 3
PEERS_GV = 4
PEER_POS = 3

TakTuk_is_ready                     =  0
TakTuk_is_numbered                  =  1
TakTuk_terminated                   =  2
connection_failed                   =  3
connection_initialized              =  4
connection_lost                     =  5
command_started                     =  6
command_failed                      =  7
command_terminated                  =  8
numbering_update_failed             =  9
pipe_input_started                  = 10
pipe_input_failed                   = 11
pipe_input_terminated               = 12
file_reception_started              = 13
file_reception_failed               = 14
file_reception_terminated           = 15
file_send_failed                    = 16
Invalid_target                      = 17
No_target                           = 18
Message_delivered                   = 19
Invalid_destination                 = 20
Destination_not_available_anymore   = 21
Wait_complete                       = 22
Wait_reduce_complete                = 23


class Spawned :
    def __init__(self, name):
        self.name = name
        self.position = "-1"
        self.rank     = -1
        self.state    = -1
        self.peers_gv = []
        self.failed   = False
        self.ready    = False
        self.me       = False
        self.wait_start   = True
        self.wait_numberr = False
        self.wait_numberu = False

    def to_string(self) :
        return "{} {} {} {} ready {} numbered {}".format(self.name, self.rank, self.state,
                self.failed, self.is_ready(), self.is_numbered())

    def update(self, position, rank, state, peers_given):
        self.position = position
        self.rank     = rank
        self.state    = state
        if self.state == TakTuk_is_ready :
            self.ready = True
        if self.state == connection_lost :
            self.make_failure()
            peers = peers_given.split(" ")
            for peer in peers:
                if peer != "" :
                    self.peers_gv.append(peer)
        return self.peers_gv

    def clean(self):
        self.peers_gv = []

    def make_failure(self) :
        self.ready  = False
        self.failed = True

    def is_ready(self) :
        return self.ready

    def is_terminated(self) :
        return self.state == TakTuk_terminated

    def is_numbering_update_failed(self) :
        return self.state == numbering_update_failed

    def connection_lost(self) :
        return self.state == connection_lost

    def is_numbered(self) :
        return self.rank != -1

    def is_failed(self) :
        return self.failed

# Class Isengard
# Author: Thomas Lavocat
#
# Global Wrapper to Taktuk
# to inherit to bring it intelligence
#
# And remember : they are taking the Hobbits to Isengard !
class Isengard:
    # Launch taktuk and the bridge socket
    # has to know if this instance is the root of all the network or not
    # because a non-root owns no taktuk !
    def __init__(self, add_to_queue, root, debug_list, loglv, logf, taktuk_path,
                 c1, c2, c3, c4, c5, taktuk_options="", log_time=None):
        self.internal = c1
        self.bisengard = c2
        self.bgeneric = c3
        self.nready = c4
        self.killn = c5
        self.taktuk_path= taktuk_path
        self.flog_time  = log_time
        self.debug_list = debug_list
        self.loglv      = loglv
        self.logf       = logf
        self.isRoot     = root
        self.spawned_lock  = Lock()
        self.spawned       = dict()
        self.wait_spawn    = []
        self.to_wait_start   = -1
        self.to_wait_numberr = -1
        self.to_wait_numberu = -1
        self.failed_update = False
        self.error_nodes   = []
        self.error_nodes_s = set()
        self.helper        = Helper()
        self.add_to_queue  = add_to_queue
        self.spawned[socket.gethostname()+"-0"]=Spawned(socket.gethostname())
        self.spawned[socket.gethostname()+"-0"].wait_start   = False
        self.spawned[socket.gethostname()+"-0"].me           = True
        self.lastHB  = time.time()
        self.running_commands = []
        if self.isRoot :
            # launch taktuk
            self.taktuk = Wrapper(self.taktuk_stdout_callback, debug_list,
                    self.loglv, self.logf, self.taktuk_path, taktuk_options,
                    None)
            # launch the bridge
            self.socket = Unix_socket(self.taktuk, self.bridge_callback,
                                      debug_list,self.loglv, self.logf)
        else :
            # launch the bridge
            self.bridge = Bridge(None, debug_list,self.loglv, self.logf, self.bridge_callback)
            self.heaBeat= Timer(consts.heartBeat, self.heart_beat)
            self.heaBeat.start()
        #callbacks
        self.ntwud_callbacks = []
        self.ntwrn_callbacks = []
        self.spawn_callbacks = []
        self.lost_callbacks  = []
        self.dead_callbacks  = []
        self.deadb_callbacks = []
        self.state = consts.INIT

    def heart_beat(self):
        kill_needed = False
        new = time.time()
        kill_needed = (int(new - self.lastHB) > (consts.heartBeat*2))
        if kill_needed :
            self.add_to_queue(Event(self.kill_needed, ""))
        else :
            self.add_to_queue(Event(self.send_heart_beat, ""))

    def send_heart_beat(self, n):
        self.send_message_to("0","all",consts.HB,True)
        self.heaBeat= Timer(consts.heartBeat, self.heart_beat)
        self.heaBeat.start()

    def kill_needed(self, n):
        logger.info(bcolors.FAIL+"parent dead"+bcolors.ENDC)
        self.killn(n)

    def log_time(self, t):
        if self.flog_time !=  None :
            self.flog_time(t)

    def send_to_taktuk(self, data):
        logger.debug("command to taktuk {}".format(data))
        if self.isRoot :
            self.socket.send_message(data)
        else :
            self.bridge._handle_message_from_socket(bytes(data, consts.encoding))

    # start taktuk and bridge thread 
    def go(self):
        if self.isRoot :
            self.taktuk.register_output_options()
            self.taktuk_built_in_regex()
            self.taktuk.start()
            self.socket.start()
            self.socket.launch_bridge()
            self.socket.ask_taktuk_infos()
        else :
            self.bridge.start()
            self.bridge.propagate_taktuk_infos()

    def taktuk_built_in_regex(self):
        # register some regex
        self.taktuk.register_callback(re.compile("^connector:.*$"), self.connector_error)
        self.taktuk.register_callback(re.compile("^state:.*$"), self.state_update)
        self.taktuk.register_callback(re.compile("^ *.* \([0-9]*, .*\)$"), self.status_print)
        self.taktuk.register_callback(re.compile("^.*[0-9]*: .*$"), self.subprocess_callback)

    #'connector=\"connector:$host;$peer;$line;$peer_position\\n\" '
    def connector_error(self, txt, txt_utf8):
        self.add_to_queue(Event(self._connector_error, txt_utf8))

    def _connector_error(self, txt):
        self.spawned_lock.acquire()
        arguments = txt.split("connector:")[1].split(";")
        logger.warning(consts.bold("connector error {}".format(arguments)))
        if ("TERM" not in arguments[2]
            and "Warning" not in arguments[2]
            and "Possible precedence issue with control " not in arguments[2] ):
            sp_node, index = self.get_wait_spawned(arguments[PEER])
            if index > -1 :
                del self.wait_spawn[index]
                self.dead_node(sp_node)
                self.propagate_state_info(sp_node)
            else :
                try:
                    sp_node_key = arguments[PEER]+"-"+arguments[PEER_POS]
                    sp_node     = self.spawned[sp_node_key]
                    nodes       = [sp_node.name]
                    ranks       = [str(sp_node.rank)]
                    childrens   = self.get_children_by_position(sp_node, arguments[PEER_POS])
                    for children in childrens :
                        self.dead_node(children)
                        nodes.append(children.name)
                        nodes.append(str(children.rank))
                    self.dead_node(sp_node)
                    self.fire_node_dead(arguments[PEER], nodes, ranks)
                    # Advertise running commandes that nodes are deads
                    toremove = []
                    dispactch= False
                    for i in range(0, len(self.running_commands)) :
                        running_command = self.running_commands[i]
                        ret = running_command.node_deads(nodes, ranks)
                        if ret > -1:
                            dispactch = True
                            if ret == 2:
                                toremove.append(i)
                                logger.info("dead command propagation")
                            break
                    if dispactch:
                        for i in toremove:
                            del self.running_commands[i]
                except :
                    pass
        self.spawned_lock.release()

    def get_children_by_position(self, father, position) :
        ret = []
        for sp_node in self.spawned.values() :
            if sp_node != father and sp_node.position.startswith(position) :
                ret.append(sp_node)
        return ret

    def dead_node(self, sp_node) :
        sp_node.make_failure()
        self.error_nodes.append(sp_node.name)
        self.error_nodes_s.add(sp_node.name)

    def spawned_list(self):
        ret = []
        for spawn in self.spawned.values():
            if spawn.name not in self.error_nodes :
                ret.append(spawn)
        return ret

    def errors_list(self) :
        return list(self.error_nodes_s)

    def get_wait_spawned(self, name) :
        logger.debug("get wait spawn {}".format(name))
        for i in range(0, len(self.wait_spawn)) :
            logger.debug("spawn {}".format(name))
            if self.wait_spawn[i].name == name :
                return self.wait_spawn[i], i
        return None, -1

    #'state="state:$host;$position;$rank;$line;$peers_given\\n"'
    def state_update(self, txt, txt_utf8):
        self.add_to_queue(Event(self._state_update, txt_utf8))

    def _state_update(self, txt):
        self.spawned_lock.acquire()
        #self.log_time("state message")
        arguments = txt.split("state:")[1].split(";")
        #logger.debug(arguments)
        sp_node_key = arguments[HOST]+"-"+arguments[POSITION]
        #logger.debug("key {}".format(sp_node_key))
        sp_node = None
        if sp_node_key in self.spawned :
            sp_node = self.spawned[sp_node_key]
        else :
            sp_node, index = self.get_wait_spawned(arguments[HOST])
            if sp_node != None :
                self.log_time("node appear")
                self.spawned[sp_node_key] = sp_node
                logger.debug("spawned nodes -> {}".format(len(self.spawned)))
                del self.wait_spawn[index]
            else :
                logger.error("this should not happen {} -> {} {}".format(txt, sp_node_key, self.spawned.keys()))
                self.spawned_lock.release()
                return
        nodes = sp_node.update(arguments[POSITION],
                       int(arguments[RANK]),
                       int(arguments[LINE]),
                       arguments[PEERS_GV])
        #handle the case where, a just start nodes is lost having jobs to
        #launch. Mark then as dead
        if len(nodes) > 0 :
            logger.debug("nodes {}".format(nodes))
            for node in nodes :
                node = node.split(":")[0]
                self.dead_node(self.spawned[node])
                self.propagate_state_info(self.spawned[node])
        self.propagate_state_info(sp_node)
        self.spawned_lock.release()

    def propagate_state_info(self, sp_node) :
        #logger.debug(sp_node.to_string())
        if sp_node.wait_start :
            if sp_node.is_ready() or sp_node.is_failed() :
            # if we were waiting for this node to start
                sp_node.wait_start = False
                self.to_wait_start -= 1
                self.log_time("node spawned")
                if self.to_wait_start > -1 and self.to_wait_start == 0 :
                    #self.log_time("network ready")
                    self.to_wait_start = -1
                    self.fire_network_ready_callback(self.error_nodes)
                    self.error_nodes = []
                    #self.log_time("network ready -> callback handled")
        elif sp_node.wait_numberr :
            if sp_node.is_numbered() or sp_node.is_numbering_update_failed() :
                sp_node.wait_numberr = False
                self.to_wait_numberr -= 1
                if self.to_wait_numberr > -1 and self.to_wait_numberr == 0 :
                    self.to_wait_numberr = -1
                    self.fire_network_renumber_callback()
        elif sp_node.wait_numberu :
            if sp_node.is_numbered() or sp_node.is_numbering_update_failed() :
                sp_node.wait_numberu = False
                if sp_node.is_numbering_update_failed() :
                    self.failed_update = True
                self.to_wait_numberu -= 1
                # if it was the last node to update, fire callback
                if self.to_wait_numberu > -1 and self.to_wait_numberu == 0 :
                    self.to_wait_numberu = -1
                    if self.failed_update :
                        self.fire_network_update_failure_callback()
                    else :
                        self.fire_network_update_success_callback()
                    self.failed_update = False
        elif sp_node.connection_lost() :
            # TODO handle the case where the connection lost is in the middle of
            # renumbering or updating
            sp_node.clean()
            self.error_nodes.append(sp_node.name)
            self.fire_connection_lost(sp_node, self.error_nodes)
            self.error_nodes = []

    def fire_connection_lost(self, sp_node, error_nodes) :
        logger.info(bcolors.FAIL+"connection_lost {} {}".format(
                    sp_node.name, error_nodes)+bcolors.ENDC)
        # self.call_lost_callbacks(sp_node, error_nodes)
        pass

    def fire_node_dead(self, sp_node, error_nodes, ranks) :
        logger.info(bcolors.FAIL+"node dead {} {}".format( sp_node.name, error_nodes)+bcolors.ENDC)
        self.call_dead_callbacks(error_nodes, errors_rank)
        pass

    def fire_network_renumber_callback(self) :
        logger.info(bcolors.FAIL+"network renumber_done"+bcolors.ENDC)
        self.call_ntwrn_callbacks()

    def fire_network_ready_callback(self, error_nodes) :
        logger.info(bcolors.FAIL+"network ready {}".format(error_nodes)+bcolors.ENDC)
        self.nready(error_nodes)
        self.call_spawn_callbacks(",".join(error_nodes))

    def fire_network_update_success_callback(self):
        logger.info(bcolors.FAIL+"network update success"+bcolors.ENDC)
        self.call_ntwud_callbacks("success")

    def fire_network_update_failure_callback(self):
        logger.info(bcolors.FAIL+"network update failure"+bcolors.ENDC)
        self.call_ntwud_callbacks("failure")

    # is called when something arrive from taktuk output
    def taktuk_stdout_callback(self, txt, txt_8) :
        logger.info(bcolors.OKBLUE+ txt_8 +bcolors.ENDC)
        pass

    def status_print(self, txt, txt_utf_8):
        self.add_to_queue(Event(self._status_print, txt_utf8))

    def _status_print(self, txt):
        # logger.info(bcolors.UNDERLINE+ txt +bcolors.ENDC)
        pass

    # shutdown the network
    def terminate(self):
        if self.isRoot :
            self.socket.shutdown()
            logger.info("socket shutdown")
            self.taktuk.shutdown()
            logger.info("taktuk shutdown")
        else :
            self.heaBeat.cancel()
            self.bridge.stop_select()

    # is called when something arrive from the bridge
    def subprocess_callback(self, txt, txt_utf8) :
        # Do not give UTF-8 to subprocess out.
        self.add_to_queue(Event(self._subprocess_callback, txt))

    def _subprocess_callback(self, txt) :
        dispactch = False
        toremove = []
        txt_base_64 = base64.b64encode(txt).decode(consts.encoding)
        txt_utf8    = txt.decode(consts.encoding)
        for i in range(0, len(self.running_commands)):
            running_command = self.running_commands[i]
            ret = running_command.test_and_dispatch(txt_utf8, txt_base_64)
            if ret > -1:
                dispactch = True
                if ret == 2:
                    toremove.append(i)
                    logger.info("end propagation")
                break
        if dispactch is False:
            logger.info(bcolors.WARNING + "SUBPROCESS {}".format(txt) + bcolors.ENDC)
        else:
            logger.debug("SUBPROCESS {}".format(txt))
            for i in toremove:
                del self.running_commands[i]

    # is called when something arrive from the bridge
    def bridge_callback(self, txt) :
        self.add_to_queue(Event(self._bridge_callback, txt))

    def _bridge_callback(self, txt) :
        try :
            obj = json.loads(txt)
            if obj[consts.FROM] == consts.INTERNAL :
                if obj[consts.TYPE] == consts.BRIDGED :
                    if self.isRoot :
                        self.bridge_dead()
                    else :
                        self.kill_needed()
                else :
                    return self.bridge_internal_callback(obj)
            elif obj[consts.TYPE] == "m" :
                try :
                    envelope     = json.loads(obj[consts.DATA])
                    local_from   = envelope[consts.MYRANK]
                    try :
                        decoded_data = json.loads(envelope[consts.DATA])
                        if decoded_data[consts.TYPE] == consts.HEARTBT:
                            logger.info("heart beat received")
                            self.send_message_to(local_from,"all",consts.HBR,True)
                        elif decoded_data[consts.TYPE] == consts.HEARTBTR:
                            logger.info("heart beat answer received")
                            self.lastHB = time.time()
                        else :
                            return self.bridge_isengard_callback(local_from,
                                                             decoded_data)
                    except ValueError :
                        return self.bridge_generic_message_callback(txt)
                except ValueError :
                    return self.bridge_generic_message_callback(txt)
        except ValueError :
            logger.warning(json.dumps({
                consts.TYPE:consts.CONTROL,
                consts.VALUE:consts.INVJSON}))

    def bridge_dead(self) :
        print(bcolors.FAIL+ "-> Bridge dead : "+bcolors.ENDC)
        self.call_dead_bridge_calllbacks()

    def bridge_internal_callback(self, obj) :
        self.internal(obj)

    def bridge_isengard_callback(self, sender, decoded_data) :
        self.bisengard(sender, decoded_data)

    def bridge_generic_message_callback(self, txt) :
        self.bgeneric(txt)

    # Commands to manipulate Taktuk ############################################

    def kill_all(self, rank):
        if self.isRoot :
            logger.info(" kill "+rank)
            self.taktuk.send_command(rank+" kill target all")

    def spawn_nodes(self, dest, nodes, synch, taktuk=True,
                    callback_object=None):
        if callback_object != None :
            self.one_shot_register_on_spawn_nodes(callback_object)
        self.spawned_lock.acquire()
        #logger.debug("spawn request received")
        # parse the asked nodes following those rules :
        for node in self.helper.build_list(nodes) :
            # create an object to follow the updates of the node
            sp_node = Spawned(node)
            self.wait_spawn.append(sp_node)
        #    logger.debug("add node {}".format(node))
            # add the nodes to the wait_start_list, this list will be used to
            # propagate the state information.
        self.to_wait_start = len(self.wait_spawn)
        #logger.debug("lock release {}".format(len(self.wait_spawn)))
        self.spawned_lock.release()
        self.final_spawn_nodes(dest, nodes, synch, taktuk)
        #logger.debug("spawn request send to taktuk")

    def final_spawn_nodes(self, dest, nodes, synch, taktuk=True):
        if taktuk :
            s = ""
            if synch :
                s = "synchronize"
            self.taktuk.send_command(s+" "+dest+" option m [ "+nodes+" ]")
        else :
            order = json.dumps({
                consts.TYPE:consts.SPAWN,
                consts.DEST:dest,
                consts.DATA:nodes,
                consts.SYNCHRO:self.get_synch(synch)
                })
            self.send_to_taktuk(order)

    def network_update(self, synch, taktuk=True, callback_object=None) :
        if callback_object != None :
            self.one_shot_register_on_network_update(callback_object)
        self.spawned_lock.acquire()
        self.to_wait_numberu = 0
        for node in self.spawned.values() :
            if not node.is_failed() :
                self.to_wait_numberu += 1
                node.wait_numberu = True
                node.rank         = -1
        if taktuk :
            s = ""
            if synch :
                s = "synchronize"
            self.taktuk.send_command(s+" network update")
        else :
            socket_order = json.dumps({
                consts.TYPE:consts.NUPDATE,
                consts.SYNCHRO:self.get_synch(synch)
                })
            self.send_to_taktuk(socket_order)
        self.spawned_lock.release()

    def network_renumber(self, synch, taktuk=True, callback_object=None) :
        if callback_object != None :
            self.one_shot_register_on_network_renumber(callback_object)
        self.spawned_lock.acquire()
        self.to_wait_numberr = 0
        for node in self.spawned.values() :
            if not node.is_failed() :
                self.to_wait_numberr += 1
                node.wait_numberr = True
                node.rank         = -1
        if taktuk :
            s = ""
            if self.get_synch(synch) :
                s = "synchronize"
            self.taktuk.send_command(s+" network renumber")
        else :
            socket_order = json.dumps({
                consts.TYPE: consts.NNUMBER,
                consts.SYNCHRO: self.get_synch(synch)
                })
            self.send_to_taktuk(socket_order)
        self.spawned_lock.release()

    def broadcast_exec(self, dest, command, synch, taktuk=True,
                       callback_object=None):
        if callback_object is not None:
            bcast = BrodcastRunningCommand(
                    self.spawned_list(),
                    command,
                    True,
                    self.loglv,
                    self.logf,
                    callback_object,
                    )
            self.running_commands.append(bcast)
        if taktuk:
            s = ""
            if synch:
                s = "synchronize"
            self.taktuk.send_command(s+" "+dest+" broadcast exec [ "
                                     + command+" ]")
        else:
            socket_order = json.dumps({
                    consts.TYPE: consts.EXECUTE,
                    consts.DEST: dest,
                    consts.DATA: command,
                    consts.SYNCHRO: self.get_synch(synch)
                    })
            self.send_to_taktuk(socket_order)

    def execute_on(self, dest, command, synch, taktuk=True,
                   callback_object=None):
        if callback_object is not None:
            rc = RunningCommand(self.get_name_of(dest),
                                dest,
                                command,
                                True, self.loglv, self.logf,
                                callback_object)
            # append command on handled running commands
            self.running_commands.append(rc)
        if taktuk:
            s = ""
            if synch:
                s = "synchronize"
            self.taktuk.send_command(s+" "+dest+" exec [ "+command+" ]")
        else:
            socket_order = json.dumps({
                    consts.TYPE: consts.EXECUTE,
                    consts.DEST: dest,
                    consts.DATA: command,
                    consts.SYNCHRO: self.get_synch(synch)
                    })
            self.send_to_taktuk(socket_order)

    def send_message_to(self, dest, target, data, synch):
        envelope = json.dumps({consts.MYRANK:self.rank,consts.DATA:data})
        socket_order = json.dumps({
                consts.TYPE:consts.MESSAGE,
                consts.DEST:dest,
                consts.TARGET:target,
                consts.DATA:envelope,
                consts.SYNCHRO:self.get_synch(synch)
                })
        # No need to send on the network a message for who I am the dest
        if self.rank == dest :
            toforward   = json.dumps({"from":self.rank, "type":"m",
                                      "data": envelope});
            self.add_to_queue(Event(self._bridge_callback, toforward))
        else :
            self.send_to_taktuk(socket_order)

    def wait_reduce(self, target, synch, taktuk=True) :
        if taktuk :
            s = ""
            if synch :
                s = "synchronize"
            self.taktuk.send_command(s+" wait reduce target "+target)
        else :
            socket_order = json.dumps({
                    consts.TYPE:consts.WAITR,
                    consts.DATA:target,
                    consts.SYNCHRO:self.get_synch(synch)
                    })
            self.send_to_taktuk(socket_order)

    def get_synch(self, synch):
        if synch :
            return consts.TRUE
        else:
            return consts.FALSE

    def get_name_of(self, rank) :
        mat = re.compile(".*-{}$".format(rank))
        for key in self.spawned.keys() :
            logger.debug("key {}".format(key))
        for key in self.spawned.keys() :
            if mat.match(key) :
                parts = key.split("-")
                name  = parts[0]
                for i in range(1, len(parts) -1) :
                    name = name+"-"+parts[i]
                return name
        else :
            return ".*"

    # callback registration
    def one_shot_register_on_network_update(self, values):
        self.ntwud_callbacks.append(values)

    def one_shot_register_on_network_renumber(self, values):
        self.ntwrn_callbacks.append(values)

    def one_shot_register_on_spawn_nodes(self, values):
        self.spawn_callbacks.append(values)

    def register_on_lost_nodes(self, values):
        self.lost_callbacks.append(values)

    def register_on_dead_nodes(self, values):
        self.dead_callbacks.append(values)

    def register_on_dead_bridge(self, values):
        self.deadb_callbacks.append(values)


    # callback call
    def call_ntwud_callbacks(self, failure) :
        for c in self.ntwud_callbacks :
            c.fire(failure)
        self.ntwud_callbacks = []

    def call_ntwrn_callbacks(self) :
        for c in self.ntwrn_callbacks :
            c.fire("")
        self.ntwrn_callbacks = []

    def call_spawn_callbacks(self, error_nodes) :
        for c in self.spawn_callbacks :
            c.fire(error_nodes)
        self.spawn_callbacks = []

    def call_lost_callbacks(self, error_nodes, errors_rank) :
        for c in self.lost_callbacks :
            c(self.rank, self.ID, error_nodes, errors_rank)

    def call_dead_callbacks(self, error_nodes, errors_rank) :
        for c in self.dead_callbacks :
            c(self.rank, self.ID, error_nodes, errors_rank)

    def call_dead_bridge_calllbacks(self) :
        for c in self.deadb_callbacks :
            c(self)

