import logging
import sys
import collections
import pexpect
from .. import consts
from threading import Thread

logger = logging.getLogger('yggdrasil')

# Class Wrapper
# Author: Thomas Lavocat
#
# Encapsulate a Taktuk execution in a pexpect spawn
# use the pexpect API to parse the output of taktuk
#
# each line of the output has to be matched with a regex in order to
# sort the Taktuk's information
class Wrapper(Thread):

    # start Taktuk
    # default_callback the default function to call if no regex has match
    def __init__(self, default_callback, debug_list, loglv, logf, taktuk_path,
                                            taktuk_options="", log_time=None):
        self.taktuk_path= taktuk_path
        self.flog_time  = log_time
        Thread.__init__(self)
        taktuk_order = ('{}taktuk '.format(self.taktuk_path)+
                        taktuk_options+
                        ' --interactive 2>&1');
        logger.info(taktuk_order)
        self.child = pexpect.spawn(taktuk_order)
        self.register_output_options()
        self.hashrf = collections.OrderedDict();
        self.default_callback = default_callback
        self.daemon = True

    def log_time(self, t):
        if self.flog_time !=  None :
            self.flog_time(t)

    def register_output_options(self):
        output  = 'connector=\"connector:$host;$peer;$line;$peer_position\\n\" '
        self.send_command("0 option o [ "+output+" ]")
        output = 'state="state:$host;$position;$rank;$line;$peers_given;\\n"'
        self.send_command("0 option o [ "+output+" ]")

    # associate a callback function with a regex
    # regex : a compiled regex
    # function : a callback function that need to take one explicit parameter
    #            (text)
    # priority by adding order (first with more priority)
    def register_callback(self, regex, function):
        self.hashrf[regex] = function;

    def run(self):
        self.run = True;
        while self.run:
            try:
                # Focus only on carriage return, according to the documentation of
                # pexpect, even on linux systems one should look for \r\n
                self.child.expect("\r\n", consts.timeout)
                # According to pexpect doc, will only fetch the current match
                stdline  = self.child.before
                stdline8 = stdline.decode(consts.encoding)
                #logger.debug(" TAKTUK LINE : "+stdline)
                # Over the regex lists
                hasMatched = False
                for key,val in self.hashrf.items() :
                    if key.match(stdline8) != None :
                        self.log_time(key.pattern)
                        val(stdline, stdline8);
                        hasMatched = True
                        break
                if not hasMatched :
                    if "option m" in stdline8 :
                        self.log_time("option m")
                    else :
                        self.log_time("taktuk output")
                    self.default_callback(stdline, stdline8)
            except pexpect.TIMEOUT:
                pass
            except pexpect.EOF:
                pass
            except Exception as e :
                logger.error("error parsing : {}".format(e))

    # send a command to taktuk
    def send_command(self, command):
        self.child.sendline(command+"\n")
        self.child.flush()

    # terminate the thread
    # latency modulo child expect timeout
    def shutdown(self):
        self.run = False
        self.send_command("quit")
        self.child.kill(9)
        self.child.kill(15)
