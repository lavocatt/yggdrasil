"""
HOSTNAMES SPECIFICATION

    Hostnames given to TakTuk might be simple machine name or complex hosts
    lists specifications. In its general form, an hostname is made of an host
    set and an optional exclusion set separated by a slash. Each of those sets
    is a comma separated list of host templates. Each of these templates is
    made of constant part (characters outside brackets) and optional range
    parts (characters inside brackets). Each range part is a comma separated
    list of intervals or single values. Each interval is made of two single
    values separated by a dash. This is true for all hostnames given to TakTuk
    (both
    with -m or -f options).

    In other words, the following expressions are valid host
    specifications:
        node0
        node[19]
        node[1-3]
        node[1-3],otherhost/node2
        node[1-3,5]part[a-b]/node[3-5]parta,node1partb

    they respectively expand to:
        node1
        node19
        node1 node2 node3
        node1 node3 otherhost
        node1parta node2parta node2partb node3partb node5partb

    Notice that these list of values are not regular expressions ("node[19]" is
    "node19" and not "node1, node2, ...., node9"). Intervals are implemented
    using the perl magical auto increment feature, thus you can use
    alphanumeric values as interval bounds (see perl documentation, operator ++
    for limitations of this auto increment).
"""


class Helper:
    def build_list(self, node_list):
        parts = node_list.split("/")
        # construct to remove list
        remove_list = []
        if len(parts) > 1:
            remove_list = self.extract_node_list(parts[1])
        # construct to keep list
        node_list = self.extract_node_list(parts[0])
        for node in remove_list:
            if node in node_list:
                node_list.remove(node)
        return node_list

    def extract_node_list(self, nodes):
        parts = []
        ret = []
        a = False
        word = ""
        for l in nodes:
            if l == "," and not a:
                parts.append(str(word))
                word = ""
            elif l == "[":
                word = word+l
                a = True
            elif l == "]":
                word = word+l
                a = False
            else:
                word = word+l
        parts.append(word)

        for part in parts:
            sub_parts     = part.split("]")
            sub_part      = sub_parts[0]
            possibilities = self.build_possibilities(sub_part)
            other_parts   = sub_parts[1:]
            for other_part in other_parts:
                to_expend         = self.build_possibilities(other_part)
                new_possibilities = self.expend_possibilities(possibilities,
                                                              to_expend)
                possibilities = new_possibilities
            ret.extend(possibilities)
        return ret

    def expend_possibilities(self, old, add):
        ret = []
        for o in old:
            for a in add:
                ret.append(o+a)
        return ret

    def build_possibilities(self, part):
        possibilities = []
        begin = part.find("[")
        if begin > -1:
            prefix  = part[:begin]
            to_adds = part[begin+1:].split(",")
            for to_add in to_adds:
                tiret = to_add.find("-")
                if tiret > -1:
                    intervals = self.build_intervals(to_add.split("-"))
                    for interval in intervals:
                        possibilities.append(prefix+interval)
                else:
                    possibilities.append(prefix+to_add)
        else:
            possibilities.append(part)
        return possibilities

    def build_intervals(self, interval):
        intervals = []
        begin = interval[0]
        end   = interval[1]
        try:
            b = int(begin)
            e = int(end)
            for i in range(b, e+1):
                intervals.append(str(i))
        except ValueError:
            b = ord(begin)
            e = ord(end)
            for i in range(b, e+1):
                intervals.append(chr(i))
        return intervals

    def compose(self, rank_list):
        ranges = []
        first = -1
        last  = -1
        for i in range(0, len(rank_list)):
            rank = rank_list[i]
            if first == -1:
                first = rank
                last  = rank
                if i == len(rank_list) -1:
                    ranges.append("{}".format(first))
            elif rank == last+1:
                last = rank
                if i == len(rank_list) -1:
                    ranges.append("{}-{}".format(first,last))
            else:
                if last != first:
                    ranges.append("{}-{}".format(first,last))
                else:
                    ranges.append("{}".format(first))
                first = rank
                last  = rank
                if i == len(rank_list) -1:
                    ranges.append("{}".format(first))
        return ranges
